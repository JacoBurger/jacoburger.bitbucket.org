(function ($) {


var _OS = {



		//************** OWL FRONT **************//

		owl_front : function() {


			$(".slider__front .slides").owlCarousel({
			 
			    // Most important owl features
			    items : 1,
			    itemsCustom : false,
			    itemsDesktop : [1199,1],
			    itemsDesktopSmall : [980,1],
			    itemsTablet: [768,1],
			    itemsTabletSmall: false,
			    itemsMobile : [479,1],
			    singleItem : false,
			    itemsScaleUp : false,
			    autoPlay : true,
    			stopOnHover : true,
			    autoplayTimeout: 900,
			    loop: true,
			    slideSpeed: 800,
			    transitionStyle : "fade",
			    animateOut: 'fadeOut',


    			slideSpeed : 900,


			    //Mouse Events
			    dragBeforeAnimFinish : true,
			    mouseDrag : true,
			    touchDrag : true,

			    // Navigation
			    navigation : true,
			    navigationText : ["prev","next"],
			    rewindNav : true,
			    scrollPerPage : false,

			    //Pagination
			    pagination : true,
			    paginationNumbers: false,
		 

				});

		},

		page_scroll : function (){
				$('#navbar').onePageNav({
			    currentClass: 'active',
			    changeHash: false,
			    scrollSpeed: 650,
			    scrollThreshold: 0.5,
			    filter: '',
			    easing: 'swing',
			    begin: function() {
			        //I get fired when the animation is starting
			    },
			    end: function() {
			        //I get fired when the animation is ending
			    },
			    scrollChange: function($currentListItem) {
			        //I get fired when you enter a section and I pass the list item of the section
			    }
			});
		},

		nav_resize : function(){
			$(window).scroll(function() {
			  if ($(document).scrollTop() > 50) {
			    $('nav').addClass('resize');
			  } else {
			    $('nav').removeClass('resize');
			  }
			});
		},

		mmenu : function(){
		      $("#my-menu").mmenu({

		      });
		      var API = $("#my-menu").data( "mmenu" );
		      
		      $("#my-button").click(function() {
		         API.open();
		      });
		},


			// INIT //
			init : function() {
				_OS.owl_front();
				_OS.page_scroll()
				_OS.nav_resize();
				_OS.mmenu();
			}


};


	$(function() {
		_OS.init();
	});


})(jQuery);

